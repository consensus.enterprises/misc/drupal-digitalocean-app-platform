[![A combination of the Drupal and DigitalOcean logos](docs/assets/logo.svg){width=128 height=128}](docs/assets/logo.md)

⚠️ This project has moved to [`neurocracy/drupal-digitalocean-app-platform`](https://gitlab.com/neurocracy/digitalocean/drupal-digitalocean-app-platform); all development continues there.

----

This is a template to get Drupal deploying on [DigitalOcean App
Platform](https://www.digitalocean.com/products/app-platform). It includes
numerous best practices for building, deploying, managing, and securing the
site. While somewhat opinionated, you're welcome to customize it as needed.

# Why?

You don't want to manage your own infrastructure and want to pay someone else to
do that, while also automating your Drupal deployments so that they only
require a couple of clicks to initiate.

You also want to minimize or eliminate your use of platforms such as
[Pantheon](https://arstechnica.com/tech-policy/2023/04/webops-platform-pantheon-defends-hosting-hate-groups-as-developers-quit/)
or any platforms that run off of Amazon, Google, or Microsoft infrastructure
([DigitalOcean runs their own
datacentres](https://en.wikipedia.org/wiki/DigitalOcean)).

## Backstory

Much of the groundwork for this was done to port
[Omnipedia](https://omnipedia.app/open-source) from a traditional VPS to App
Platform at a time when documentation, articles, and so on for doing so for a
somewhat complex Drupal site was essentially non-existent. Our hope with this is
that we can save you time and effort while simultaneously promoting various best
practices.

# What about Aegir 5?

[Aegir 5 is happening](https://consensus.enterprises/blog/aegir5-is-happening/),
but it's not there yet at the time of writing. Aegir 5 is intended to offer
multiple back-ends via an extensible plug-in architecture which will support
external hosting providers; [DigitalOcean has an extensive
API](https://docs.digitalocean.com/reference/api/api-reference/) purpose-made
for such uses. In short, if you move your sites to DigitalOcean App Platform,
you'll likely be able to manage them via Aegir 5 in the future with minimal
changes.

# Deploying

Here's a nice and shiny link to prepopulate the App Platform create app process
with this repository. Note that you'll need a DigitalOcean account, and you'll
need to fill in various environment variables.

[![Deploy to DO](https://www.deploytodo.com/do-btn-blue.svg)](https://cloud.digitalocean.com/apps/new?repo=https://gitlab.com/consensus.enterprises/misc/drupal-digitalocean-app-platform/tree/main)

This is currently intended for preview purposes but the long-term goal is to
automate this even further to minimize the need to edit things manually,
ideally as a series of standalone scripts and as a back-end to [Aegir
5](https://gitlab.com/aegir/aegir).

# Documentation

1. [Background tasks and cron](docs/background-tasks.md)
2. [Troubleshooting](docs/troubleshooting.md)
