The [logo](logo.svg) for this repository is a combinaton of the [the Drupal drop
logo from the Drupal media kit](https://www.drupal.org/about/media-kit/logos) and [the DigitalOcean logo from Wikimedia
Commons](https://commons.wikimedia.org/wiki/File:DigitalOcean_icon.svg), edited
together in [Inkscape](https://inkscape.org). These are used with the intent of
promoting both Drupal and DigitalOcean and should not be reused in any way that
violates their respective licenses.
