# Build error: Yarn v2+ is not supported

If your build is failing with an error similar to:

> Yarn v2+ is not supported on this App Platform stack.

and you're already using the most recent [Node.js
buildpack](https://docs.digitalocean.com/products/app-platform/languages-frameworks/nodejs/),
it does indeed support Yarn 3 and 4 but `heroku/nodejs-engine` is stuck at an
older version; the solution is to add this to your [app
spec](https://docs.digitalocean.com/glossary/app-spec/) to temporarily downgrade
the buildpack stack:

```yaml
features:
  - buildpack-stack=ubuntu-18
```

Then upload it and let it deploy (it'll fail but that's expected), then change
the above to:

```yaml
features:
  - buildpack-stack=ubuntu-22
```

Then upload it and let it deploy which it should now do so successfully with
Yarn 3 or 4.

The reason this seems to work is that App Platform is stuck on an older
`heroku/nodejs-engine` if you created the app on an older version of that
buildpack and this prompts it to upgrade everything, including the buildpacks
not directly upgradeable in the UI.

Special thanks to DigitalOcean support who provided this solution.
