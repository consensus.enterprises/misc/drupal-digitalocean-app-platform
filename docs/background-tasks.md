Drupal core ships with [an automated cron
module](https://www.drupal.org/docs/administering-a-drupal-site/cron-automated-tasks/cron-automated-tasks-overview) that's' good enough for
basic sites that receive some regular traffic, which runs cron tasks at end of
a request, after data has been sent to a user's browser. The limitation of this
approach is that running cron is entirely at the mercy of when and how often
requests are made to your site. If this is good enough for your use-case, you
can enable the Automated Cron module and ignore the rest of this.

# Simple but regular cron tasks

[We provide a DigitalOcean function that can ping your site's cron URL at a set
interval](https://gitlab.com/consensus.enterprises/misc/drupal-digitalocean-function-cron);
please see that repository for instructions on setting it up.

# Longer-running tasks

If you need to perform longer-running tasks, such as processing queues, we recommend adding [a
worker](https://docs.digitalocean.com/products/app-platform/how-to/manage-workers/) to your app, which can handle these without the time
restrictions of a web request. We may add this to this template in the future,
but for the time being you can look to [Omnipedia's background
tasks](https://github.com/neurocracy/omnipedia/blob/9.x/.do/run-background-tasks.php) for reference to implement your own.
